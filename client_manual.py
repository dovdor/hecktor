import datetime
import serial
import pygame
import time
import json
import os

INPUT = 0
OUTPUT = 1

LOW = 0
HIGH = 1

A0 = 14

class SerialClient(object):
	def __init__(self, port = 'COM7'):
		print "Connecting to %s..." % port
		self._conn = serial.Serial(port, 115200, timeout=1, write_timeout=1)
		time.sleep(3) # wait for init to complete
		print "Connected!"

	def command(self, cmd, *params, **kwargs):
		try:
			expect = kwargs.get('expect')

			command_line = "\n".join([cmd] + map(str, params)) + "\n"
			
			print "writing", repr(command_line)

			try:
				self._conn.write(command_line)
			except:
				pass
			# time.sleep(0.1)

			# response = self._conn.read(200).strip()
			# print "response", response

			# if expect is not None and response != expect:
			# 	raise Exception("Invalid response from %s command. Expected %s, got %s" % (cmd, expect, response))

			# return response
		except:
			print "Error writing command"

g_state = 'stop'

def main():
	client = SerialClient()

	pygame.init()
	screen = pygame.display.set_mode((400, 300))

	try:
		while True:
			for event in pygame.event.get():
				pass

			keys = pygame.key.get_pressed()

			if keys[pygame.K_q]:
				break

			if keys[pygame.K_UP]:
				_set_state(client, "forward")
			elif keys[pygame.K_DOWN]:
				_set_state(client, "backward")
			elif keys[pygame.K_RIGHT]:
				_set_state(client, "right")
			elif keys[pygame.K_LEFT]:
				_set_state(client, "left")
			else:
				_set_state(client, "stop")
			
			pygame.display.flip()
			#time.sleep(0.1)
	finally:
		client.command("stop")

def _set_state(client, new_state):
	global g_state
	if new_state != g_state:
		g_state = new_state
		client.command(new_state)

if __name__ == "__main__":
	main()

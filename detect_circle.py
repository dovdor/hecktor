import urllib
import numpy as np
import sys
sys.path.append('/usr/local/lib/python2.7/site-packages')
import cv2


# Replace the URL with your own IPwebcam shot.jpg IP:port
url='http://172.16.27.74:8080/shot.jpg'


FILENAME = 'resources/easy.jpg'


def read_image(filename):
	img = cv2.imread(filename)
	return img


def filter_blue(frame):
	frame = cv2.resize(frame, (0, 0), fx=0.5, fy=0.5)
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	# define range of blue color in HSV
	lower_blue = np.array([110, 50, 50])
	upper_blue = np.array([130, 255, 255])

	# Threshold the HSV image to get only blue colors
	mask = cv2.inRange(hsv, lower_blue, upper_blue)
	cv2.imshow('raw', frame)
	cv2.imshow('hsv', frame)
	cv2.imshow('mask', mask)
	cv2.waitKey(0)

	# Bitwise-AND mask and original image
	res = cv2.bitwise_and(frame, frame, mask=mask)
	return res


def find_circles(img):
	img = cv2.medianBlur(img, 5)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	img = cv2.GaussianBlur(img, (5, 5), 0)
	# img = cv2.threshold(img, 10, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
	cv2.imshow('hecktor_bw', img)
	return cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 2, minDist=1000, param1=100,
		param2=150, minRadius=0, maxRadius=0)


def display(img, circles):
	if circles is None:
		cv2.imshow('hecktor', img)
		return
	circles = np.uint16(np.around(circles))
	for i in circles[0, :]:
		# draw the outer circle
		cv2.circle(img, (i[0], i[1]), i[2], (0, 255, 0), 2)
		# draw the center of the circle
		cv2.circle(img, (i[0], i[1]), 2, (0, 0, 255), 3)
	cv2.imshow('hecktor', img)


def go_image(filename=FILENAME, should_display=True):
	circles, img = detect_circles(filename)
	if should_display:
		display(img, circles)
		cv2.waitKey(0)
	return circles, img


def detect_circles(filename):
	img = read_image(filename)
	# img = cv2.resize(img, (0, 0), fx=0.5, fy=0.5)
	circles = find_circles(img)
	return circles, img


# put the image on screen
	# cv2.imshow('IPWebcam',img)

	# To give the processor some less stress
	# time.sleep(0.1)

def go():
	while True:
		# Use urllib to get the image and convert into a cv2 usable format
		imgResp = urllib.urlopen(url)
		imgNp = np.array(bytearray(imgResp.read()),dtype=np.uint8)
		img = cv2.imdecode(imgNp,-1)
		img = cv2.resize(img, (0, 0), fx=0.5, fy=0.5)
		circles = find_circles(img)
		display(img, circles)
		# put the image on screen
		# cv2.imshow('IPWebcam',img)

		#To give the processor some less stress
        #time.sleep(0.1) 

		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

if __name__ == '__main__':
	# find_circle(read_image())
	go_image()

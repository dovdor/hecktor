import unittest

from detect_circle import go_image

DISPLAY = True


class TestNavigate(unittest.TestCase):

	def test_easy(self):
		circles, img = go_image('../resources/easy.jpg', should_display=DISPLAY)
		self.assertIsNotNone(circles)
		self.assertEquals(1, len(circles))

	def test_gilboy(self):
		circles, img = go_image('../resources/gilboy.jpg', should_display=DISPLAY)
		self.assertIsNotNone(circles)
		self.assertEquals(1, len(circles))

	def test_bright_bg(self):
		circles, img = go_image('../resources/bright_bg.jpg', should_display=DISPLAY)
		self.assertIsNotNone(circles)
		self.assertEquals(1, len(circles))

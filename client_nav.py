from navigate import Navigator, Actions

import datetime
import serial
import pygame
import time
import json
import random
import os

import urllib
import numpy as np
import sys
sys.path.append('/usr/local/lib/python2.7/site-packages')
import cv2

INPUT = 0
OUTPUT = 1

LOW = 0
HIGH = 1

A0 = 14

url='http://172.16.27.74:8080/shot.jpg'

class SerialClient(object):
	def __init__(self, port = 'COM7'):
		print "Connecting to %s..." % port
		self._conn = serial.Serial(port, 115200, timeout=1, write_timeout=1)
		time.sleep(3) # wait for init to complete
		print "Connected!"

	def command(self, cmd, *params, **kwargs):
		try:
			expect = kwargs.get('expect')
			command_line = "\n".join([cmd] + map(str, params)) + "\n"
			
			# print "writing", repr(command_line)

			try:
				self._conn.write(command_line)
			except:
				pass

		except:
			# print "Error writing command"
			pass

class Robot(object):
	TURNING_TIME = 0.8

	def __init__(self):
		self.client = client = SerialClient()
		pygame.init()
		screen = pygame.display.set_mode((400, 300))

		print "a"
		self.navigator = Navigator("corridor1.json")
		print "b`"
		self.route = self.navigator.navigate('Starting', '2')
		print 'ROUTE: ', self.route

		self.move_state = 'stop'

	def go(self):
		try:
			while True:
				for event in pygame.event.get():
					pass

				keys = pygame.key.get_pressed()
				if keys[pygame.K_q]:
					break

				print "1"
				if self._go_to_next_circle():
					print "FOUND CIRCLE"
					nxt = self._get_next_command()
					if nxt == 'turn_right':
						self._turn_right()
					elif nxt == 'turn_left':
						self._turn_left()
					elif nxt == 'done':
						print "DONE!!!"
						return

				pygame.display.flip()
				#time.sleep(0.1)
		finally:
			self.client.command("stop")

	def _get_next_command(self):
		if not self.route:
			return 'done'

		command = self.route[1]
		self.route = self.route[2:]

		if command == (Actions.ROTATE_RIGHT, 1):
			return 'turn_right'
		if command == (Actions.ROTATE_RIGHT, 3):
			return 'turn_left'

		assert False, 'Unknown command: %s' % command

	def _turn_right(self):
		self._move_n_wait('right', self.TURNING_TIME)

	def _turn_left(self):
		self._move_n_wait('left', self.TURNING_TIME)

	def _go_to_next_circle(self):
		command = get_directions()
		print "GO TO CIRCLE COMMAND: %s" % command

		if command == 'done':
			return True

		if command in ("right", "left", "forward"):
			self._move_n_wait(command, 0.5 if command == 'forward' else 0.1)
		elif command == 'search':
			self._move_n_wait(random.choice(["right", "left"]), 0.1)

		return False

	def _move_n_wait(self, command, moving_time, wait_time = 0.4):
		self._move(command)
		time.sleep(moving_time)
		self._move("stop")
		time.sleep(wait_time) # let the image stabilize

	def _move(self, command):
		if command != self.move_state:
			self.move_state = command
			self.client.command(command)


# Replace the URL with your own IPwebcam shot.jpg IP:port

PIVOT_TH = 0.15
FORWARD_TH = 0.4

def get_directions():
	imgResp = urllib.urlopen(url)
	imgNp = np.array(bytearray(imgResp.read()),dtype=np.uint8)
	img = cv2.imdecode(imgNp,-1)
	circle_x, circle_y, circle_radius = find_circle(img)
	
	if circle_y is None:
		return "search"

	height, width, _ = img.shape
	width = float(width)
	mid = width / 2
	distance = circle_radius * 2.0 / width

	pivot = (circle_x - mid) / width
	# print "circle_x:", circle_x
	# print "circle_radius:", circle_radius
	# print "height:", height
	# print "width:", width
	# print "mid:", mid
	# print "pivot:", pivot
	# print "distance:", distance

	if pivot > PIVOT_TH:
		return "right"
	elif pivot < -PIVOT_TH:
		return "left"
	elif distance < FORWARD_TH:
		return "forward"

	return "done"

def find_circle(img):
	img = cv2.medianBlur(img, 5)
	gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	circles = cv2.HoughCircles(gray_img, cv2.HOUGH_GRADIENT, 1, 100, param1=500,
		param2=30, minRadius=0,    maxRadius=0)
	
	center = None, None, None

	if circles is not None:
		circles = np.uint16(np.around(circles))
		for i in circles[0, :]:
			# draw the outer circle
			cv2.circle(img, (i[0], i[1]), i[2], (0, 255, 0), 2)
			# draw the center of the circle
			cv2.circle(img, (i[0], i[1]), 2, (0, 0, 255), 3)

			center = (i[0], i[1], i[2])

	img = cv2.resize(img, (0,0), fx=0.5, fy=0.5)
	cv2.imshow('detected circles', img)

	return center

if __name__ == "__main__":
	Robot().go()
